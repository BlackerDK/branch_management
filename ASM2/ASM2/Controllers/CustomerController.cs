﻿using ASM2.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace ASM2.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class CustomerController : ControllerBase
	{
		private readonly MyDbContextcs _context;
		public CustomerController(MyDbContextcs context)
		{
			_context = context;
		}
		[HttpGet]
		public IActionResult Get()
		{
			var dsCust = _context.customers.ToList();
			return Ok(dsCust);
		}
		[HttpGet("{id}")]
		public IActionResult GetById(int id)
		{
			var dsCust = _context.customers.SingleOrDefault(p => p.BranchId == id);
			if (dsCust != null)
			{
				return Ok(dsCust);
			}
			return NotFound();
		}
		[HttpPost]
		public IActionResult CreateNew(Customer create)
		{
			_context.Add(create);
			_context.SaveChanges();
			return Ok();
		}
		[HttpPut("{id}")]
		public IActionResult UpdateById(int id, Customer model)
		{
			if (id == model.BranchId || model.BranchId != null)
			{
				_context.Update(model);
				_context.SaveChanges();
				return Ok();
			}
			else
			{
				return NotFound();
			}
		}
		[HttpDelete("{id}")]
		public IActionResult DeleteById(int id)
		{
			var dsCust = _context.customers.SingleOrDefault(p => p.BranchId == id);
			if (dsCust != null)
			{
				_context.Remove(dsCust);
				_context.SaveChanges();
				return NoContent();
			}
			return NotFound();
		}
	}
}
