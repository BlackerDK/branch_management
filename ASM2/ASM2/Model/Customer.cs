﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASM2.Model
{
	[Table("ManagementCustomer")]
	public class Customer
	{
		[Key]
		public int BranchId { get; set; }
		[Required]
		[MaxLength(100)]
		public string Name { get; set; }
		[MaxLength(100)]
		public string Address { get; set; }
		[MaxLength(100)]
		public string City { get; set; }
		[MaxLength(100)]
		public string State { get; set; }
		[MaxLength(100)]
		public string ZipCode { get; set; }
	}
}
