﻿using Microsoft.EntityFrameworkCore;

namespace ASM2.Model
{
	public class MyDbContextcs :DbContext
	{
		public MyDbContextcs(DbContextOptions options):base(options) { }
		#region DbSet
		public DbSet<Customer> customers { get; set; }
		#endregion
	}
}
