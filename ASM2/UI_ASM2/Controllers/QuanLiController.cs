﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.AccessControl;
using UI_ASM2.Models;
using Newtonsoft.Json;

namespace UI_ASM2.Controllers
{
	public class QuanLiController : Controller
	{
		Uri baseAddress = new Uri("https://localhost:44307/api");
		private readonly HttpClient _client;
		public QuanLiController()
		{
			_client = new HttpClient();
			_client.BaseAddress = baseAddress;
		}
		[HttpGet]
		public IActionResult Index()
		{
			List<QuanLiBanHang> dsBanHang = new List<QuanLiBanHang>();
			HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/Customer").Result;
			if (response.IsSuccessStatusCode)
			{
				string data = response.Content.ReadAsStringAsync().Result;
				dsBanHang=JsonConvert.DeserializeObject<List<QuanLiBanHang>>(data);
			}
			return View(dsBanHang);
		}
	}
}
