﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace UI_ASM2.Models
{
	public class QuanLiBanHang
	{
		[Key]
		[DisplayName("Mã chi nhánh")]
		public int BranchId { get; set; }
		[Required]
		[MaxLength(100)]
		[DisplayName("Tên chi nhánh")]
		public string Name { get; set; }
		[DisplayName("Địa chỉ")]
		[MaxLength(100)]
		public string Address { get; set; }
		[MaxLength(100)]
		[DisplayName("Thành phố chi nhánh")]
		public string City { get; set; }
		[MaxLength(100)]
		[DisplayName("Trạng thái chi nhánh")]
		public string State { get; set; }
		[MaxLength(100)]
		[DisplayName("Mã bưu chính chi nhanh")]
		public string ZipCode { get; set; }
	}
}
